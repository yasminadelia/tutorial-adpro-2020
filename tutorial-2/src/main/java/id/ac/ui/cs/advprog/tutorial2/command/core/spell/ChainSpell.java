package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    private ArrayList<Spell> spellList;

    public ChainSpell(ArrayList<Spell> spellList){
        this.spellList = spellList;
    }

    public void cast(){
        for(Spell spell : spellList){
            spell.cast();
        }

    }

    public void undo(){
        for(int i=spellList.size()-1; i>=0; i--){
            spellList.get(i).undo();
        }

    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
