package id.ac.ui.cs.advprog.tutorial5.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "soul")
public class Soul {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    // TODO: Lengkapi atribut yang sudah anda rencanakan. Ingat bahwa atribut yang dibuat bersifat privat.
    private String gender;
    private int age;
    private String occupation;

    public Soul(){}
    public Soul(long id, String name, int age, String gender, String occupation){
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.occupation = occupation;

    }

    public void setID(long newId){
        id = newId;
    }
    public void setName(String newName){
        name = newName;
    }
    public void setGender(String newGender){
        gender = newGender;
    }
    public void setAge(int newAge){
        age = newAge;
    }
    public void setOccupation(String newOccupation){
        occupation = newOccupation;
    }

    public long getId(){
        return id;
    }

    public String getName(){
        return name;
    }
    public String getGender(){
        return gender;
    }

    public int getAge(){
        return age;
    }

    public String getOccupation(){
        return occupation;
    }
}

