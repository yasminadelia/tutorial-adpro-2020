package id.ac.ui.cs.advprog.tutorial5.repository;


import org.springframework.data.jpa.repository.JpaRepository;

// TODO: Import Soul.java
import id.ac.ui.cs.advprog.tutorial5.core.Soul;

public interface SoulRepository extends JpaRepository<Soul, Long> {
}
