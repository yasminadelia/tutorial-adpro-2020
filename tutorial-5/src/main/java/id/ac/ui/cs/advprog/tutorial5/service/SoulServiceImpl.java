package id.ac.ui.cs.advprog.tutorial5.service;

// TODO: Import service bean
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service

    @Autowired
    SoulRepository sr;

    public SoulServiceImpl(SoulRepository sr){
        this.sr = sr;
    }

    public List<Soul> findAll(){
        return sr.findAll();
    }
    public Optional<Soul> findSoul(Long id){
        return sr.findById(id);
    }
    public void erase(Long id){
        sr.deleteById(id);
    }
    public Soul rewrite(Soul soul){
        return sr.save(soul);

    }
    public Soul register(Soul soul){
        return sr.save(soul);
    }
}

