import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class SoulTest{

    Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul(1, "Salsa", 11, "Female", "Student");
    }

    @Test
    public void testGetterMethod(){
        assertEquals(1, soul.getId());
        assertEquals("Salsa", soul.getName());
        assertEquals(11, soul.getAge());
        assertEquals("Female", soul.getGender());
        assertEquals("Student", soul.getOccupation());
    }

    public void testSetterMethod(){
        soul.setID(2);
        soul.setName("Tayo");
        soul.setAge(17);
        soul.setGender("Male");
        soul.setOccupation("Bus");

        assertEquals(1, soul.getId());
        assertEquals("Tayo", soul.getName());
        assertEquals(17, soul.getAge());
        assertEquals("Male", soul.getGender());
        assertEquals("Bus", soul.getOccupation());
    }
}