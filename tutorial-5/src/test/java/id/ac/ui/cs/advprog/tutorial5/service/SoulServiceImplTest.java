package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository sr;

    @InjectMocks
    private SoulServiceImpl ssi;

    @Test
    public void whenFindAllIsCalledItShouldRunFindAll(){
        ssi.findAll();
        verify(sr,times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldRunFindByID(){
        long id = 1;
        ssi.findSoul(id);

        verify(sr, times(1)).findById(id);
    }

    @Test
    public void whenEraseIsCalledItShouldRunDeleteByID(){
        long id = 1;
        ssi.erase(id);

        verify(sr, times(1)).deleteById(id);
    }

    @Test
    public void whenRewriteIsCalledItShouldUpdateSoul(){
        Soul soul = new Soul (1, "Salsa", 11, "Female", "Student");
        ssi.rewrite(soul);

        verify(sr, times(1)).save(soul);
    }

    @Test
    public void whenRegisterIsCalledItShouldRunSave(){
        Soul soul = new Soul (1, "Salsa", 11, "Female", "Student");
        ssi.register(soul);

        verify(sr, times(1)).save(soul);
    }


}
