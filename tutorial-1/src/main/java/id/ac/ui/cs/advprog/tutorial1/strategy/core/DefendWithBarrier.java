package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Defended with barrier";
    }

    public String getType(){
        return "barrier";
    }
}
