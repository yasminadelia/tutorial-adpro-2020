package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete
    // Agile Adventurer secara default memiliki
    // attack behaviour menggunakan Senjata Api dan defense behaviour Penghadang(Barrier).

    @Override
    public String getAlias() {
        return "Agile";
    }

    public AgileAdventurer(){
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());

        AttackBehavior attackB = new AttackWithGun();
        DefenseBehavior defenseB = new DefendWithBarrier();
    }
}
