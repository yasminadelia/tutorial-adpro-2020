package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
    //Mystic Adventurer secara default memiliki attack behaviour
    //menggunakan Sihir dan defense behaviour menggunakan Perisai.

    @Override
    public String getAlias() {
        return "Mystic";
    }

    public MysticAdventurer(){
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());

        AttackBehavior attackB = new AttackWithMagic();
        DefenseBehavior defenseB = new DefendWithShield();

    }
}
