package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
    //Knight Adventurer secara default memiliki attack behaviour
    //menggunakan Pedang dan defense behaviour Armor.

    @Override
    public String getAlias() {
        return "Knight";
    }

    public KnightAdventurer(){
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());

        AttackBehavior attackB = new AttackWithSword();
        DefenseBehavior defenseB = new DefendWithArmor();

    }


}
